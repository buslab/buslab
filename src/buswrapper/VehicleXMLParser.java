/*
 * Josiah Yoder
 * Winter 2014-2015
 *
 * Based on code from Jay Urbain
 */
package buswrapper;

/*
 * @author Jay Urbain
 *
 * XML SAX Parser for weather.com locations
 * 
 * Inputs: XML file:
	See example:
	http://wxdata.weather.com/wxdata/weather/local/USWI0455?unit=e&dayf=5&cc=*
	
	Usage example:
	WeatherDotComLocationXMLParser x = new WeatherDotComLocationXMLParser();
	x,parse( xmlString);
	List<WeatherDotComLocation> list = x.getList();
 * 
 * Date: 1/5/2014
 */

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;


public class VehicleXMLParser extends DefaultHandler {

    private String chars;

    // The following variables hold values read from the file until a VehicleText object can be created.
    /**
     * Vehicle ID -- a number
     */
    private String vid = "";
    /**
     * Vehicle latitude (degrees)
     */
    private String lat = "";
    /**
     * Vehicle longitude (degrees)
     */
    private String lon = "";

    /**
     * Timestamp (see VehicleText for more info)
     */
    private String tmstmp = "";

	public List<VehicleText> vehicleList;

	/*
 	Querying http://realtime.ridemcts.com/bustime/api/v1/getvehicles?key=_____________________&rt=GRE
	*/

	public VehicleXMLParser() throws Exception {
		super();
	}

	//===========================================================
	// SAX DocumentHandler methods
	//===========================================================

	public void startDocument() throws SAXException {

		//System.out.println("START DOCUMENT File");
		vehicleList = new ArrayList<VehicleText>();
	}

	public void endDocument() throws SAXException {

	}

	public void startElement(String namespaceURI, String lName, // local name
			String qName, // qualified name
			Attributes attrs) throws SAXException {
        String elementName;
        Attributes attributes;

		String eName = lName; // element name
		if ("".equals(eName))
			eName = qName; // namespaceAware = false
		elementName = eName;

		if (attrs != null) {
			attributes = attrs;
		}

		chars = "";
	}

	public void endElement(String namespaceURI, String sName, // simple name
			String qName // qualified name
			) throws SAXException {

		String eName = sName; // element name
		if ("".equals(eName))
			eName = qName; // namespaceAware = false

		if(eName.equals("vid")) {
			vid = chars;
		}

		if(eName.equals("lat")) {
			lat = chars;
		}

		if(eName.equals("lon")) {
			lon = chars;
		}

        if(eName.equals("tmstmp")) {
            tmstmp = chars;
        }

		if(eName.equals("vehicle")) {
			VehicleText vehicleText = new VehicleText(vid, tmstmp, lat, lon);
			vehicleList.add(vehicleText);
		}
	}

	public void characters(char buf[], int offset, int len) throws SAXException {

		try {
			StringBuffer sb = new StringBuffer();
			for(int i=0; i<len; i++) {
				chars += buf[i+offset];
			}
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void parseXML(String xml) {

		// create file
//		String fname = "weather.xml";
//		File file = new File(fname);
//		file.delete();
//        try {
//        	BufferedWriter writer = new BufferedWriter(new FileWriter(fname, true));
//	        writer.write(queryFile);
//	        writer.newLine();
//	        writer.close();
//        }
//        catch(IOException e) {
//        	System.out.println(e);
//        }

		// Use the default (non-validating) parser
		SAXParserFactory factory = SAXParserFactory.newInstance();
		factory.setValidating(false);
		try {
			// Parse the input
			SAXParser saxParser = factory.newSAXParser();
			saxParser.parse(new InputSource(new StringReader(xml)), (DefaultHandler) this);
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	public List<VehicleText> getForecastList() {
		return vehicleList;
	}
}