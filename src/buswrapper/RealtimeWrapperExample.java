/*
 * Josiah Yoder
 * Winter 2014-2015
 *
 * Based on code by Dr. Jay Urbain
 */
package buswrapper;

import java.util.List;

/**
 * Example usage of the RealtimeWrapper class.
 */
public class RealtimeWrapperExample {
	/**
	 * Example usage of the RealtimeWrapper class.
	 */
	public static void main(String[] args) {
        try {
            RealtimeWrapper wrapper = new RealtimeWrapper("key.txt");
            List<VehicleText> vehicles = wrapper.fetchVehicles();
            System.out.println(vehicles);
        } catch(RealtimeWrapperException e) {
            System.out.println("I was unable to fetch the information from the server");
            e.printStackTrace();
        }
	}
}
