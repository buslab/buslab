/*
 * @author Jay Urbain, Josiah Yoder
 * Date: 1/5/2014
 */

package buswrapper;

/**
 * This class represents the raw text describing a position of a single bus.
 * The web API wrapper will return some sort of collection of these representing all the vehicles on the route.
 *
 * You probably don't want to use this as your state object...
 */
public class VehicleText {
    /**
     * Vehicle ID -- a number
     */
	public final String vid;
    /**
     * Timestamp when information was captured (e.g. 20150115 10:23)
     */
    public final String tmstmp;
    /**
     * Vehicle latitude (degrees)
     */
	public final String lat;
    /**
     * Vehicle longitude (degrees)
     */
    public final String lon;

    /**
     * @param vid Vehicle ID - a number
     * @param lat Vehicle latitude (degrees)
     * @param lon Vehicle longitude (degrees)
     * @param tmstmp Timestamp (e.g. 20150115 10:23)
     */
    public VehicleText(String vid, String tmstmp, String lat, String lon) {
        this.vid = vid;
        this.tmstmp = tmstmp;
        this.lat = lat;
        this.lon = lon;
    }

    /**
     * @return String representation of the VehicleText object.
     *   e.g. [vid:3483, lat:42.39548201923, lon: -87.904839823498234]
     */
    public String toString() {
        return "[vid:"+vid+", tmstmp:"+tmstmp+", lat:"+lat+", lon:"+lon+"]";
    }
}
