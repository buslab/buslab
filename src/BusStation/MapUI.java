/** 
 * Course: SE2811
 * Term: Winter 201_-201_
 * Assignment: Lab 4
 * Author: Dan Baumgart
 * Date: 1/11/2015
 */
package BusStation;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EtchedBorder;

import edu.msoe.se1010.winPlotter.WinPlotter;

/**
 * The User Interface for the program. Displays a checkbox for each tracked bus. 
 * Contains a text display to hold the buses information. 
 * @author baumgartd
 *
 */
public class MapUI extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private ArrayList<Integer>buses;
	private JPanel showBuses;
	private JPanel buttonPanel;
	private JPanel showText;
	private JTextArea textData;
	private WinPlotter plotter;
	private JButton toggleMap;
	private JButton updateText;
	private BusSubject bus;
	
	/**
	 * The constructor for the UI. Takes a bus object in order to obtain tracked 
	 * buses. Calls the helper method to create the UI.
	 * @param bus
	 */
	public MapUI(BusSubject bus){
		this.bus = bus;
		createContents();
		plotter = new WinPlotter();
		plotter.setVisible(false);
	}
	
	/**
	 * The helper method that creates and places all the components of the UI.
	 */
	public void createContents(){
		
		
		textData = new JTextArea();
		toggleMap = new JButton("Toggle Map");
		updateText = new JButton("View Bus Description");
		showBuses = new JPanel();
		showText = new JPanel();
		buttonPanel = new JPanel();
		
		this.setLayout(new BorderLayout());
		showBuses.setLayout(new BoxLayout(showBuses,BoxLayout.Y_AXIS));
		buttonPanel.setLayout(new FlowLayout());
		
		showBuses.setBorder(new EtchedBorder(1));
		showText.setBorder(new EtchedBorder(1));
		buttonPanel.setBorder(new EtchedBorder(1));
		
		textData.setEditable(false);
		textData.setText("No bus is currently selected.");
		
		
		buttonPanel.add(toggleMap);
		buttonPanel.add(updateText);
		showText.add(textData);
		
		
		this.setSize(700,700);
		this.add(buttonPanel, BorderLayout.SOUTH);
		this.add(showBuses,BorderLayout.WEST);
		this.add(showText,BorderLayout.CENTER);
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		createJCheckBoxes();
		this.setResizable(false);
		this.setVisible(true);
	}
	
	/**
	 * Set the actionlistener for the map button.
	 * @param a
	 */
	public void setMapButtonAction(ActionListener a){
		toggleMap.addActionListener(a);
	}
	
	/**
	 * Set the actionlistner for the text update button.
	 * @param a
	 */
	public void setUpdateText(ActionListener a){
		updateText.addActionListener(a);
	}
	
	/**
	 * 
	 * @return - the WinPlotter associated with the UI.
	 */
	public WinPlotter getPlotter(){
		return plotter;
	}
	
	/**
	 * Allows the observer to handle updating the text.
	 * @return - the textArea whos display will be updated.
	 */
	public JTextArea getTextField(){
		return textData;
	}
	
	/**
	 * Dynamically create a JCheckBox for each tracked bus. Associates the buses
	 * VID with the box that matches it's name.
	 */
	private void createJCheckBoxes(){
		buses = bus.getVIDS();
		int vid;
		for (int i = 1;i <buses.size(); i++){
			vid = buses.get(i);
			JCheckBox jcb = new JCheckBox("Bus "+i + " VID: " +vid);
			jcb.setActionCommand(String.valueOf(vid));
			showBuses.add(jcb);
		
		}
		
	}
	
	/**
	 * Runs through the panel of checkboxes and returns an array of each selected bus
	 * returning it's VID.
	 * @return
	 * @throws NumberFormatException
	 */
	public Integer[] getSelectedBus() throws NumberFormatException{
		Integer[] selectedBuses = new Integer[buses.size()];
		
		int count =0 ;
		for (Component c: showBuses.getComponents()){
			if (c instanceof JCheckBox){
				JCheckBox selected = (JCheckBox)c;
					if (selected.isSelected()){
						selectedBuses[count] = Integer.parseInt(selected.getActionCommand());
						count++;
					}
			}
		}
		return selectedBuses;
	}
	
	
	
}
