/** 
 * Course: SE2811
 * Term: Winter 2014-2015
 * Assignment: Lab 5
 * Author: Lillian Horn (hornl)
 */
package BusStation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

/**
 * This class will update the Text on the UI and calculate the velocity of the 
 * selected Buses
 * @author hornl
 *
 */
public class TextObserver extends AbstractBusObserver{
	//A table of the previous bus data, to calculate the velocity
	private Hashtable<Integer, Bus> oldBusTable;	
	
	//A table of the current bus data
	private Hashtable<Integer, Bus> newBusTable;
	
	//The GUI object which will have its text changed
	private MapUI map;
	
	//The String to print to the GUI
	private String data= "No bus is currently selected.";
	
	//The formatter that carries the form that the date/ time is in, used for making the DateTime object. 
	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd HH:mm");
	
	//The list of all of the bus vins
	private ArrayList<Integer> allVins;
	
	private NumberFormat numFormat = new DecimalFormat("#0.000");
	
	/**
	 * creates an instance of the TextObserver class, initializing the UI object
	 * and adding the actionListener to the Display Bus button. 
	 * @param m the UI object
	 */
	public TextObserver(MapUI m){
		map = m;
		newBusTable = new Hashtable<Integer, Bus>();
		map.setUpdateText(new ActionListener(){

			/**
			 * calls the Update method to update the text on the GUI
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				updateData();
			}});
		
	}
	

	@Override
	/**
	 * This method will update the values of all the busses that are on the route. 
	 */
	public synchronized void update( Hashtable<Integer, Bus> busTable, ArrayList<Integer> vins) {

		//set old and new bus data
		oldBusTable = new Hashtable<Integer, Bus>(newBusTable);
		newBusTable = new Hashtable<Integer, Bus>(busTable);
		updateData();
	}
	/**
	 * Will update all of the text data by looping through the selected busses and adding them to the text data
	 */
	private synchronized void updateData(){
		//reset data string
		data="";
		Integer[] vinSelec = map.getSelectedBus();
		for( int i = 0; i<vinSelec.length; i++ ){
			if(!(vinSelec[i]==null)){
				addBus(vinSelec[i]);
			}
		}
		
		//Have Swing update the GUI
			SwingUtilities.invokeLater(new Runnable(){
				/**
				 * will update GUI
				 */
				@Override
				public void run() {
					display();

				}

			});
	}



	/**
	 * This method will update the GUI with the new values for the busses
	 */
	@Override
	public void display() {
		JTextArea text = map.getTextField();
		text.setText(data);
		
	}
	
	/**
	 * This method will calculate the current velocity of the  specified bus
	 * @param east	the current position east of the courthouse
	 * @param north	the current position north of the courthouse
	 * @param time	the current time of the measurement
	 * @param key	The vin of the bus to calculate
	 * @return		The velocity of the bus
	 */
	//TODO: Calculate correctly
	private double calculateVelocity(double east, double north, String time, int key){
		if(!(oldBusTable==null)&&!(oldBusTable.isEmpty()) &&oldBusTable.containsKey(key)){
			Bus b = oldBusTable.get(key);
			
			// Calculate the distance of the bus
			double distance = Math.sqrt(Math.pow((east - b.getPositionEast()), 2) + Math.pow((north - b.getPositionNorth()), 2));
			
			//Find the last time of the bus
			double oldTime = convertString(b.getTimeStamp());
			
			//find the new time of the bus
			double newTime = convertString(time);
			double timeDiff = newTime - oldTime;
			
			//velocity = distance/time
			return distance/(timeDiff/3600); 
		}else{
			return 0;
		}

	}

	/**
	 * This class formats the string into seconds from the epoch 
	 * @param time	the time formatted as: 20150115 10:23
	 * @return		the time in seconds from the epoch
	 */
	private double convertString(String time){
		// Make a localDateTime object from the string using the formatter
		LocalDateTime t = LocalDateTime.parse(time, formatter);
		return t.toEpochSecond(ZoneOffset.ofHours(-6));
	}
	
	/**
	 * This method will add the text values of a single bus to the table of values to be displayed
	 * @param vin the number of the bus to be added
	 */
	public void addBus(int vin){
		Bus b = newBusTable.get(vin);
		//calculate velocity
		double velocity = this.calculateVelocity(b.getPositionEast(), b.getPositionNorth(), b.getTimeStamp(), vin);
		
		String toPass = "Bus " + vin + "\nPosition east of Courthouse: " + numFormat.format(b.getPositionEast()) + "\nPosition north of Courthouse: "
				+ numFormat.format(b.getPositionNorth()) + "\nVelocity of Bus: " + numFormat.format(velocity) + " miles/hour";
		
		 data= data+ toPass + "\n";
	}

}