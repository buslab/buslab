package BusStation;

import java.util.ArrayList;
import java.util.Hashtable;

public abstract class AbstractBusObserver {
	
	public abstract void update(Hashtable<Integer,Bus> hash, ArrayList<Integer> vins);
	
	/**
	 * This class will tell the MapUI whether or not to show that type of panel output. 
	 * For the Map Observer, that means the map would toggle visible or not 
	 * @param b the state of visibility for the map / text. 
	 */
	public abstract void display();
}
