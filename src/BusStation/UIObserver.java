/** 
 * Course: SE2811
 * Term: Winter 2014-2015
 * Assignment: Lab 5
 * Author: Lillian Horn (hornl)
 */

package BusStation;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Hashtable;
import edu.msoe.se1010.winPlotter.WinPlotter;

/**
 * This class will draw and update the Map UI whenever the toggle map button is pressed 
 * or the data is updated
 * @author hornl
 *
 */
public class UIObserver extends AbstractBusObserver{
	//The UI object that contains the WinPlotter instance
	private MapUI map; 
	//A Hashmap of the current positions of all the busses
	private Hashtable<Integer, Bus> buses;
	//An array of all the vins of the busses
	private ArrayList<Integer> vins;
	//The current state of the Map window
	private boolean visible = false;
	//This observer instance
	private UIObserver thisObserver;
	//The WinPlotter instance
	private WinPlotter p;
	
	/**
	 * Will create an instance of the UIObserver class, set up the WinPlotter size, 
	 * and the ActionListener for the ToggleMap button
	 * @param map	The GUI that has the ToggleMap button and the WinPlotter instance
	 */
	public UIObserver(MapUI map){
		
		thisObserver = this;
		this.map = map;
		//obtain and set up a plotter
		p = map.getPlotter();
		WinPlotter p = map.getPlotter();
		p.setWindowSize(1000, 1000);
		p.setPlotBoundaries(-50, -50, 50, 50);
		//Set up an action listener for the ToggleMap button
		map.setMapButtonAction(new ActionListener(){

			/**
			 * Will call the observer's display method and update the map
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				thisObserver.display();
				thisObserver.update(buses, vins);
				
				}});
	}

	/**
	 * Will update the map GUI with the positions of the selected busses
	 */
	@Override
	public void update( Hashtable<Integer, Bus> busTable, ArrayList<Integer> vins) {
		// set bus positions and vin Array
		buses = busTable;
		this.vins = vins;
		
		//reset map then draw Grid and courthouse
		p.erase();
		p.setGrid(true, 5, 5, Color.BLUE);
		p.setPenColor(0, 10, 0);
		p.drawPoint(0, 0);
		p.printAt(0, 0, "Milwaukee Court House");
		
		//get all the selected vehicles, then add them to the map
		Integer[] vinSelec = map.getSelectedBus();
		for( int i = 0; i<vinSelec.length; i++ ){
			if(!(vinSelec[i]==null)){
				Bus b = busTable.get(vinSelec[i]);
				p.drawPoint(b.getPositionEast()*5, b.getPositionNorth()*5); //set point
				p.printAt(b.getPositionEast()*5-5, b.getPositionNorth()*5+1, "Vin: "+ vinSelec[i]); //set vin number near point
			}
		}	
		p.setWindowTitle("Map View of All Busses");	//set title of window
		
	}
	
	/**
	 * Will set the map to the opposite visibility that it currently has
	 */
	@Override
	public void display() {
		map.getPlotter().setVisible(!visible);
		visible = !visible;
	}

}
