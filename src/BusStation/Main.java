/** 
 * Course: SE2811
 * Term: Winter 201_-201_
 * Assignment: Lab 4
 * Author: Dan Baumgart
 */

package BusStation;

import java.util.Timer;
import java.util.TimerTask;

/**
 * The driver for the program. Creates a subject and the observers. Passes the 
 * observers a MapUI object to update their needed components. Sets a timer to 
 * update the bus information every 30 seconds.
 * @author baumgartd
 *
 */
public class Main {
	
	public static void main(String[] args){
		BusSubject bus = new BusSubject();
		MapUI map = new MapUI(bus);
		UIObserver uiObserver = new UIObserver(map);
		TextObserver textObserver = new TextObserver(map);
		bus.add(uiObserver);
		bus.add(textObserver);
		bus.getBusData();
		
		/**
		 * Update the bus information every 30 seconds.
		 */
		Timer timer = new Timer();
		timer.schedule(new TimerTask(){

			@Override
			public void run() {
				bus.getBusData();
				
			}
			
		},30000,30000);
	}
}
