/** 
 * Course: SE2811
 * Term: Winter 201_-201_
 * Assignment: Lab 4
 * Author: Dan Baumgart
 * Date: 1/21/2015
 */
package BusStation;


/**
 * Describes the methods that each Subject must contain. It is left up to 
 * the child class to implement.
 * @author baumgartd
 *
 */
public abstract class AbstractBusSubject {
	
	public abstract boolean add(AbstractBusObserver o);
	public abstract boolean remove(AbstractBusObserver o);
	public abstract void notifyObserver();
	
}
