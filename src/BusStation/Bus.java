/** 
 * Course: SE2811
 * Term: Winter 201_-201_
 * Assignment: Lab 4
 * Author: Dan Baumgart
 * Date: 1/11/2015
 */
package BusStation;

/**
 * Represents a bus object that will contain all the needed information for any
 * tracked bus.
 * @author baumgartd
 *
 */
public class Bus {

	private int VID;
	private String timeStamp;
	private double longitude;
	private double latitude;
	private double positionNorth;
	private double positionEast;
	
	/**
	 * The constructor for a bus.
	 * @param VID
	 * @param timeStamp
	 * @param longitude
	 * @param latitude
	 */
	public Bus(int VID, String timeStamp, 
			   double longitude,double latitude,
			   double positionNorth,double positionEast){
		this.VID = VID;
		this.timeStamp = timeStamp;
		this.longitude = longitude;
		this.latitude = latitude;
		this.positionEast = positionEast;
		this.positionNorth = positionNorth;
		
	}

	@Override
	public String toString() {
		return "Bus [VID=" + VID + ", timeStamp=" + timeStamp + ", longitude="
				+ longitude + ", latitude=" + latitude + ", positionNorth="
				+ positionNorth + ", positionEast=" + positionEast + "]";
	}

	/**
	 * 
	 * @return - The buses vehicle ID number
	 */
	public int getVID() {
		return VID;
	}

	/**
	 * Set the buses vehicle ID number
	 * @param vID
	 */
	public void setVID(int vID) {
		VID = vID;
	}

	/**
	 * 
	 * @return - the current time at last update
	 */
	public String getTimeStamp() {
		return timeStamp;
	}

	/**
	 * Set the current time
	 * @param timeStamp
	 */
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	/**
	 * 
	 * @return - the buses current longitude
	 */
	public double getLongitude() {
		return longitude;
	}

	/**
	 * Set the buses current longitude
	 * @param longitude
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	/**
	 * 
	 * @return - the buses current latitude
	 */
	public double getLatitude() {
		return latitude;
	}

	/**
	 * Set the buses current latitude
	 * @param latitude
	 */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	/**
	 * Set the buses current position east of city hall in miles.
	 * @param pos
	 */
	public void setPositionEast(double pos){
		positionEast = pos;
	}
	
	/**
	 * 
	 * @return - in miles, the buses position east of city hall.
	 */
	public double getPositionEast(){
		return positionEast;
	}
	
	/**
	 * Set the buses current position north of city hall, in miles.
	 * @param pos
	 */
	public void setPositionNorth(double pos){
		positionNorth = pos;
	}
	
	/**
	 * 
	 * @return - the buses current position north of city hall, in miles.
	 */
	public double getPositionNorth(){
		return positionNorth;
	}
	
}
