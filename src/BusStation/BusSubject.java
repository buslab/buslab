/** 
 * Course: SE2811
 * Term: Winter 201_-201_
 * Assignment: Lab 4
 * Author: Dan Baumgart
 */
package BusStation;

import java.io.File;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import buswrapper.RealtimeWrapper;
import buswrapper.RealtimeWrapperException;
import buswrapper.VehicleText;

/**
 * Represent a BusSubject object. It is responsible for obtaining the information
 * for each bus from a RealTimeWrapper object. Converting the longitude and 
 * latitude to miles north and east of the city hall. It also must keep track
 * of all subscribed observers.
 *
 */

public class BusSubject extends AbstractBusSubject {

	//Contains all subscribed observers
	private Collection<AbstractBusObserver> observers;

	//Each bus and it's associated vehicle id number (VID)
	private Hashtable<Integer, Bus> busTable;

	private RealtimeWrapper rw;

	//1 degree of latitude in miles
	private static final double LATITUDEMILES = 69.03;

	//1 degree of longitude in miles
	private static final double LONGITUDEMILES = 50.63;

	//City hall's latitude
	private static final double CITYHALLLATITUDE = 43.04173;

	//City hall's longitude
	private static final double CITYHALLLONGITUDE = -87.90976;

	//A list of each vehicle id number for tracked buses
	private ArrayList<Integer>vids;	


	/**
	 * Constructor for a BusSubject object. Creates a RealtimeWrapper object.
	 * Attempts to get the bus data using that object.
	 */
	public BusSubject(){
		try{
			rw =  new RealtimeWrapper("D:\\MyDocs\\Documents\\2014-2015\\SE2811\\BusLab\\buslab\\src\\BusStation\\key.txt");	//For LiLy, whose computer is impared...D:\\MyDocs\\Documents\\2014-2015\\SE2811\\BusLab\\buslab\\src\\BusStation\\
		}catch(RealtimeWrapperException e) {
			System.out.println(e.getMessage());
		}

		busTable = new Hashtable<Integer,Bus>();
		observers = new ArrayList<AbstractBusObserver>();
		vids = new ArrayList<Integer>();

		try{
			getBusData();
		}catch (NumberFormatException e){
			System.out.println(e.getMessage());
		}

	}

	/**
	 * Allows an observer to subscribe for updates. Add's it to a list if not 
	 * already contained in the list. 
	 */
	@Override
	public boolean add(AbstractBusObserver o) {
		if (!(observers.contains(o))){
			observers.add(o);
			return true;
		}
		return false;
	}

	/**
	 * Removes an observer from the list, canceling updates. 
	 */
	@Override
	public boolean remove(AbstractBusObserver o) {
		if (!(observers.contains(o))){
			return false;
		}else{
			observers.remove(o);
			return true;
		}

	}

	/**
	 * Notifies all observers that data has been updated and passes that data to
	 * the observer.
	 */
	@Override
	public void notifyObserver() {
		
		for(AbstractBusObserver observer : observers){
			observer.update(busTable, vids);
		}

	}
	

	/**
	 * Calculates the miles north of city hall from the buses latitude.
	 * @param lat
	 * @return position north 
	 */
	private double milesNorth(double lat){
		
		return(lat - CITYHALLLATITUDE)*LATITUDEMILES;

	}

	/**
	 * Calculates the miles east of city hall from the buses longitude.
	 * @param lng
	 * @return position east
	 */
	private double milesEast(double lng){
		return (lng - CITYHALLLONGITUDE)*LONGITUDEMILES;
	}

	/**
	 * A list of all tracked vehicle ID numbers
	 * @return - ArrayList of VIDS
	 */
	public ArrayList<Integer> getVIDS(){

		return vids;
	}

	/**
	 * Gets the data from the RealtimeWraper object. Creates a bus object for each
	 * and adds it to the Hashtable or updates it's information based on it's VID.
	 * @throws NumberFormatException
	 */
	public void getBusData() throws NumberFormatException{
		//Returned from the RealtimeWrapper objects fetchVehicles method
		List<VehicleText>vehicles = null; 

		//Holds the info for each bus
		String[] bus = new String[3];
		int VID;
		String time;
		double lng;
		double lat;

		//Grab the info for each bus
		try{

			vehicles = rw.fetchVehicles();

		}catch (RealtimeWrapperException e){
			System.out.println(e.getMessage());
		}

		if (!(vehicles == null)){
			for (VehicleText text: vehicles){

				//Remove all brackets and split into separate string on a ,
				bus = text.toString().replaceAll("^\\[|\\]$", "").split(",");

				//Grab the values for each bus object
				VID = Integer.parseInt(getLast(bus[0]));
				time = parseTime(bus[1]); 
				lng = Double.parseDouble(getLast(bus[2]));
				lat = Double.parseDouble(getLast(bus[3]));

				//On initial load
				if (!(busTable.contains(VID))){
					busTable.put(VID, new Bus(VID,time,lng,lat, milesNorth(lng)
								 ,milesEast(lat)));
					vids.add(VID); 
					
				}else{
					//For all updates
					Bus b = busTable.get(VID);
					b.setLatitude(lat);
					b.setLongitude(lng);
					b.setTimeStamp(time);
					b.setPositionEast(milesEast(lat));
					b.setPositionNorth(milesNorth(lng));
					
				}

			}
			notifyObserver();

		}
	}

	/**
	 * Split a string and return the value after the split.
	 * @param s
	 * @return
	 */
	private String getLast(String s){
		String[] string = s.split(":");
		return string[1];

	}
	
	/**
	 * Split a string and return the time value after the split.
	 * Requires special parsing as the values before and after the 
	 * colon are needed.
	 * @param s
	 * @return
	 */
	private String parseTime(String s){
		String [] string = s.split(":");
		return string[1] + ":"+string[2];
	}
}